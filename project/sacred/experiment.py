import os, json, logging, numpy as np

from project.settings import EXPERIMENTS_DIR, MODULE_DIR
from project.sacred.utils import config_logger

from project.model import model_fn, train_fn, input_fn
from project.sacred.ingredients import data_ingredient, load_data

from keras.callbacks import Callback

from sacred import Experiment
from sacred.observers import FileStorageObserver
from sacred.utils import apply_backspaces_and_linefeeds


# define our sacred expirment (ex) and add our data_ingredient
ex = Experiment('demo', interactive=True, ingredients=[data_ingredient])
# provide the configurable parameters from the JSON config file.
ex.add_config(os.path.join(MODULE_DIR, 'model', 'config.json'))

# ensure the directory to which we write exists
if not os.path.isdir(os.path.join(EXPERIMENTS_DIR)): os.makedirs(EXPERIMENTS_DIR)
ex.observers.append(FileStorageObserver.create(EXPERIMENTS_DIR))
ex.capture_out_filter = apply_backspaces_and_linefeeds

# use python logger
ex.logger = logging.getLogger(__name__)


@ex.capture
def metrics(_run, _config, logs):
    '''
    Arguments:
        _run (sacred _run): the current run
        _config (sacred _config): the current configuration
        logs (keras.logs): the logs from a keras model

    Returns: None
    '''
    _run.log_scalar('loss', float(logs.get('loss')))
    if _config["classification"]:
        _run.log_scalar("acc", float(logs.get('accuracy')))
    else:
        _run.log_scalar("mse", float(logs.get('mean_squared_error')))
        _run.log_scalar("mae", float(logs.get('mean_absolute_error')))
        _run.log_scalar("mape", float(logs.get('mean_absolute_percentage_error')))
        _run.log_scalar("cosine", float(logs.get('cosine_proximity')))



class LogMetrics(Callback):
    '''
    A wrapper over the capture method `metrics` to have keras's logs be
    integrated into sacred's log.
    '''
    def on_epoch_end(self, _, logs={}):
        metrics(logs=logs)


@ex.automain
def main(_log, _run, _config):
    '''
    Notes:
        variables starting with _ are automatically passed via sacred due to
        the wrapper.

        I prefer to return, at most, a single value. The returned value will be
        stored in the Observer (file or mongo) and if large weight matricies or
        the model itself, will be very inefficient for storage. Those files
        should be added via 'add_artifact' method.

    Arguments:
        _log (sacred _log): the current logger
        _run (sacred _run): the current run
        _config (sacred _config): the current configuration file

    Returns:
        result (float): accuracy if classification, otherwise mean_squared_error
    '''
    # the subdirectory for this particular experiment
    run_dir = os.path.join(EXPERIMENTS_DIR, str(_run._id))

    # inform the logger to dump to run_dir
    config_logger(run_dir)

    _log.debug('Loading data')
    (twins, map_twin_a, map_twin_b) = load_data(
        save_dir = run_dir,
        _config  = _config,
        _run     = _run,
        _log     = _log
    )
    _log.debug('Data loaded')

    _log.debug('Preparing data generator function')
    data = input_fn(
        data   = (twins, map_twin_a, map_twin_b),
        config = _config,
        _run   = _run,
        _log   = _log
    )
    _log.debug('Data generator function prepared')

    _log.debug('Wiring model')
    model = model_fn(
        _run   = _run,
        _log   = _log,
        config = {
            'dims_twin_a': len(map_twin_a['elem']),
            'dims_twin_b': len(map_twin_b['elem']),
            **_config
        }
    )
    _log.debug('Model wired')

    _log.debug('Call training loop')
    trained = train_fn(
        _run   = _run,
        _log   = _log,
        model  = model,
        data   = data,
        config = {
            'steps': len(twins) // _config['positive_examples'],
            'callbacks': [LogMetrics()],
            **_config
        }
    )
    _log.debug('Training complete')


    # if running 100s of models this can quickly eat up your disk. Only save if
    # you are sure you want to save the results. You can always re-run with best
    # model and save that one instance
    if _config['save']:
        _log.debug('Saving model')
        model.save(os.path.join(run_dir, 'model.h5'))
        _log.debug('Model saved')

    if _config['classification']:
        result = trained.history['accuracy'][-1]
    else:
        result = trained.history['mean_squared_error'][-1]
    return result
