def train_fn(model, data, config:dict, _run=None, _log=None):
    '''
    Arguments:
        model: the model which is to be trained. In this example it is the
            `.keras.twin_embedding_model`.

        data: the data on which to train. In this example it is a generator
            function `.keras.generate_batched_twins`.

        config (dict): parameters which may influence training

        _run (sacred run): the _run object from a sacred experiment. By default
            None.

        _log (sacred log): the _log object from a sacred experiment. By default
            None.

    Returns:
        results: trained model
    '''
    if _log is not None: _log.debug('Training model')
    callbacks = config['callbacks'] if config['callbacks'] else []
    trained = model.fit_generator(
        data,
        epochs          = config["epochs"],
        steps_per_epoch = config['steps'],
        callbacks       = callbacks
    )
    return trained
