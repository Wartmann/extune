from .keras import twin_embedding_model

def model_fn(config:dict, _run=None, _log=None):
    '''
    Arguments:
        config (dict): configuration for your model
        _run (sacred run): the _run object from a sacred experiment. By default
            None.
        _log (sacred log): the _log object from a sacred experiment. By default
            None.
    Returns:
        model: a trainable model
    '''
    if _log is not None: _log.debug('Making model')
    model = twin_embedding_model(
        dims_twin_a = config["dims_twin_a"],
        dims_twin_b = config["dims_twin_b"],
        dims_latent = config["latent_factors"],
        name_twin_a = config["name_twin_a"],
        name_twin_b = config["name_twin_b"],
        classification = config["classification"],
    )
    if _log is not None: _log.debug('Model made.')

    return model
